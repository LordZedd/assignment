OBJECTIVE

Collaborative Work between Okolo Michael and Nwisu Chisom on Assignment 2 of MN404:  
Write an Algorithm/ flowchart and then convert it to a java program for given game of NIM.

REQUIREMENTS

Purpose of the assessment (with ULO Mapping)
This assignment assesses the following Unit Learning Outcomes; 
students should be able to demonstrate their achievements in them.

c.	Apply principles of abstraction and problem solving in an object-oriented programming language 
d.	Apply knowledge of programming constructs in developing computer programs
e.	Create programs based on incremental development processes of designing, coding, testing and debugging.

INSTRUCTION

	All work must be submitted on Moodle by the due date along with the completed Assignment Cover Page. 
	The assignment must be in MS Word format, 1.5 spacing, 11-pt Calibri (Body) font and 2 cm margins on all four sides of your page with appropriate section headings.  
	Reference sources must be cited in the text of the report and listed appropriately at the end in a reference list using IEEE referencing style.
	Algorithm/flow chart to be submitted in a word doc1.
	Draft copy and final version of the program to be submitted in a word doc2 and doc3 respectively.
	Both file names must have student Id as part of the file names. 
	Students must ensure before submission of final version of the assignment that the similarity percentage as computed by Turnitin has to be less than 10%.
